<?php 
   
   namespace App\Entity

   class_cadastros {

     /**
      * Identificador unico de cadastros
      *  @var integer 
      */

      public $id;

      /**
       * nome do usuario
       * @var string
       */

      public $nome;

       /**
       * sobrenome do usuario
       * @var string
       */

      public $sobrenome;

            /**
       *email do usuario
       *@var string
       */

      public $Email;


            /**
       * endereço do usuario
       * @var string
       */

      public $rua;

            /**
       * endereço do usuario
       * @var string
       */

      public $bairro;

            /**
       * endereço do usuario
       * @var string
       */

      public $numero;

            /**
       * endereço do usuario
       * @var string
       */

      public $complemento;

            /**
       * endereço do usuario
       * @var string
       */

      public $cidade;

            /**
       * endereço do usuario
       * @var string
       */

      public $estado;

            /**
       * endereço do usuario
       * @var string
       */

      public $CEP;

            /**
       * endereço do usuario
       * @var string (s/n)
       */

      public $ativo;


            /**
       * Observações do usuario
       * @var string
       */

      public $observacoes;
      
     /**
       * data cadastro do usuario
       * @var string
       */

      public $data;

   }

?>