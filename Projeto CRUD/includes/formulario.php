<main>
    <section>
        <a href="index.php">
            <button class="btn btn-success">Voltar</button>
        </a>

        <h2 class="mt-3">Cadastrar Usuario</h2>

    <form method="POST">
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputEmail4">Nome</label>
                    <input type="text" class="form-control" name="nome" id="inputEmail4" placeholder="Nome">
                    </div>

                    <div class="form-group col-md-6">
                    <label for="inputEmail4">SobreNome</label>
                    <input type="text" class="form-control" name="sobrenome" id="inputEmail4" placeholder="SobreNome">
                    </div>

                    <div class="form-group col-md-6">
                    <label for="inputPassword4">E-mail</label>
                    <input type="text" class="form-control" name="Email" id="inputEmail4" placeholder="E-mail">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputAddress">Rua</label>
                    <input type="text" class="form-control" name="rua" id="inputAddress" placeholder="Rua , Avenida , Alameda , etc">
                </div>

                <div class="form-group">
                    <label for="inputAddress2">Bairro</label>
                    <input type="text" class="form-control" name="bairro" id="inputAddress2" placeholder="Ex: Vila Matilde">
                </div>

                <div class="form-group col-md-6" style="padding-left: 0px;">
                    <label for="inputAddress2">Número</label>
                    <input type="text" class="form-control" name="numero" id="inputAddress2" placeholder="N°" style="width: 30%;">
                </div>

                <div class="form-group">
                    <label for="inputAddress2">Complemento</label>
                    <input type="text" class="form-control" name="complemento" id="inputAddress2" placeholder="Casa , Apartamento , Complexo">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputCity">Cidade</label>
                    <input type="text" class="form-control" name="cidade" id="inputCity">
                    </div>
                    <div class="form-group col-md-4">
                    <label for="inputEstado">Estado</label>
                    <select name="estado" id="inputEstado" class="form-control">
                        <option selected>Escolher...</option>
                        <option>São Paulo</option>
                    </select>
                    </div>

                    <div class="form-group col-md-2">
                    <label for="inputCEP">CEP</label>
                    <input type="text" class="form-control" name="CEP" id="inputCEP">
                    </div>
                </div>

                <div class="form-row">
                       <label for="" style="margin-right:10px;margin-left: 5px;">Status</label>
                       
                       <div>
                           <div class="form-check form-check-inline">
                               <label for="" class="form-control">
                                   <input type="radio" name="ativo" id="" value="s" checked>Ativo
                               </label>
                           </div>
                       </div>

                       <div>
                           <div class="form-check form-check-inline">
                               <label for="" class="form-control">
                                   <input type="radio" name="ativo" id="" value="n">Inativo
                               </label>
                           </div>
                       </div>
                    </div>

                 <div class="form-group">
                    <label for="">Observações</label>
                    <textarea class="form-control" name="observacao" id=""  rows="5"></textarea>
                 </div>

                 <div class="form-group">
                    <label for="exampleFormControlFile1">Anexo de arquivo</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                </div>

                <div class="form-group">
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck">
                    <label class="form-check-label" for="gridCheck">
                        Clique em mim
                    </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Entrar</button>
        </form>
    </section>
</main>